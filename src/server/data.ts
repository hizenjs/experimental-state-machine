import { Types, User, Article } from 'types'

export const types: Types[] = ['Hardware', 'Mobile', 'Server', 'System', 'Website']

export const users: User[] = [
  { name: 'Angela Sanders',
    avatar: 'https://i.imgur.com/QYVXAws.png',
    job: 'UIX Designer'
  },
  { name: 'Joseph Stewart',
    avatar: 'https://i.imgur.com/p8ZLrCd.png',
    job: 'Web Software Engineer'
  },
  {
    name: 'Mary Williams',
    avatar: 'https://i.imgur.com/FSVVtxP.png',
    job: 'Product Owner'
  },
  { name: 'Tiffany Hanks',
    avatar: 'https://i.imgur.com/3ZEbmj7.png',
    job: 'Creative Director'
  },
]

export const data: { [k:string]:Article } = {
  0: {
    id: '123',
    title: 'Adding animated navigating to your website',
    illustration: 'https://images.unsplash.com/photo-1504983886798-4a6ed8c55497?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2068&q=80',
    date: '20 October 2021',
    category: 'Website',
    author: {
      name: 'Tiffany Hanks',
      avatar: 'https://i.imgur.com/3ZEbmj7.png',
      job: 'Creative Director'
    }
  },
  1: {
    id: '321',
    title: 'Adding animated navigating to your website',
    illustration: 'https://images.unsplash.com/photo-1565793979206-10951493332d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1964&q=80',
    date: '19 October 2021',
    category: 'Mobile',
    author: {
      name: 'Joseph Stewart',
      avatar: 'https://i.imgur.com/p8ZLrCd.png',
      job: 'Web Software Engineer'
    }
  },
  2: {
    id: '456',
    title: 'Adding animated navigating to your website',
    illustration: 'https://images.unsplash.com/photo-1505761671935-60b3a7427bad?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80',
    date: '18 October 2021',
    category: 'Server',
    author: {
      name: 'Angela Sanders',
      avatar: 'https://i.imgur.com/QYVXAws.png',
      job: 'UIX Designer'
    }
  },
  3: {
    id: '654',
    title: 'Adding animated navigating to your website',
    illustration: 'https://images.unsplash.com/photo-1517458047551-6766fa5a9362?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80',
    date: '17 October 2021',
    category: 'System',
    author: {
      name: 'Mary Williams',
      avatar: 'https://i.imgur.com/FSVVtxP.png',
      job: 'Product Owner'
    }
  },
}