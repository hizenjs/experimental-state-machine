import { data } from 'server/data'
import time from 'services/time'
import { Context } from 'types'

export default function fakeServer(context: Context, duration: number): Promise<Context> {
  for (const id in context) {
    if (JSON.stringify(context[id]) !== JSON.stringify(data[id])) context[id].date = time.current
  }
  return new Promise(resolve => window.setTimeout(() => { resolve(context) }, duration))
}