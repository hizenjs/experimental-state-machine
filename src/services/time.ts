const timestamp = new Date();
const day = timestamp.getDate();
const month = timestamp.toLocaleString('default', { month: 'long'});
const year = timestamp.getFullYear();

const time: {timestamp: Date, current: string} = {
  current: `${day} ${month} ${year}`,
  timestamp,
}

export default time