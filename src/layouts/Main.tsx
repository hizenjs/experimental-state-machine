import React, { ReactElement, ReactNode } from 'react'
import styled from 'styled-components'

interface Props {
  children: ReactNode
}

const OldElPasso = styled.div`
  padding: 44px 0;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #101010;
  color: #FFFFFF;
  text-align: center;
  font-family: Arial, sans-serif;
`

export default function Main ({ children }: Props): ReactElement {
  return <OldElPasso>{children}</OldElPasso>
}
