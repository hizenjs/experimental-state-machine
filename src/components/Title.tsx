import React, { ReactElement } from 'react'
import { Input, Title as Text } from 'styles'
import { ON } from 'machine/article'

const Title = (props): ReactElement => {
  const {isEdit, isAllowed, send, article, index} = props
  if (isEdit) return (
    <Input
      rows={2}
      disabled={!isAllowed(ON.CHANGE_TITLE)}
      defaultValue={article.title}
      onChange={({ target }) =>
        send(ON.CHANGE_TITLE, { value: target.value, index: index + 1 })}
    />
  )
  return <Text>{article.title}</Text>
}

export default Title