import React from 'react'
import { Author, Category, Illustration, Edit, Title } from 'components'
import { ArticleComponent, ArticleProps } from 'types'
import { Container, Top, Content, State} from 'styles'
import schema, { STATE } from 'machine/article'
import { useArticle, useMachine } from 'hooks'

export default function Article ({ articleId, index }: ArticleProps): ArticleComponent {
  const {state, send, context, isAllowed} = useMachine(schema)
  const isEdit = ![STATE.IDLE, STATE.SUCCESS].includes(state)
  const article = useArticle(context, articleId)

  const props = {article, articleId, index, isAllowed, send, isEdit}

  return (
    <Container flip={false} >
      <Illustration {...article} />
      <State>{state}</State>
      <Content>
        <Top>
          <Category {...props} />
          <Edit {...props} />
        </Top>
        <Title {...props} />
        <Author {...props} />
      </Content>
    </Container>
  )
}

