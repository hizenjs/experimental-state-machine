import React, { ReactElement } from 'react'
import styled, { keyframes } from 'styled-components'

interface Props {
  title: string
  subtitle: string
}

const SlideInDownTitle = keyframes`
  0% {
    opacity: 0;
    transform: translateY(-70px) rotateX(90deg) scale(0.5);
  }
  100% {
    opacity: 1;
  }
`

const SlideInDownSubTitle = keyframes`
  0% {
    opacity: 0;
    transform: translateY(-100px) scale(0.2);
  }
  100% {
    opacity: 1;
  }
`

const Container = styled.div`
  margin-bottom: 44px;
`

const Title = styled.h1`
  font-weight: 300;
  font-size: 50px;
  margin: 0;
  //animation: ${SlideInDownTitle} 1.5s ease-in-out;
`
const Subtitle = styled.h2`
  font-weight: 300;
  font-size: 20px;
  text-transform: uppercase;
  letter-spacing: 2px;
  //animation: ${SlideInDownSubTitle} 1.5s ease-in-out;
`

export default function Heading ({ title, subtitle }: Props): ReactElement {
  return (
    <Container>
      <Title>{title}</Title>
      <Subtitle>{subtitle}</Subtitle>
    </Container>
  )
}
