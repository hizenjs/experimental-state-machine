import React, { ReactElement } from 'react'
import { Cover } from 'styles'


const Illustration = ({ illustration, title }): ReactElement => (
  <Cover src={illustration} alt={title}/>
)
export default Illustration
