import { Date, Info, Select, Type } from 'styles'
import React from 'react'
import { ON } from 'machine/article'
import { types } from 'server/data'

const Category = (props) => {
  const {article, send, index, isEdit} = props
  if (isEdit) return (
    <Info>
      <Select
        defaultValue={article.category}
        onChange={({target}) =>
          send(ON.CHANGE_CATEGORY, { value: target.value, index })
        }>
        { types.map(type => <option key={type} value={type}>{type}</option>) }
      </Select>
    </Info>
  )
 return (
   <Info>
    <Type>{article.category}</Type>
    <Date>Last Update: {article.date}</Date>
  </Info>
 )
}

export default Category