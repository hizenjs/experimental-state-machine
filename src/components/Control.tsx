import React, { ReactElement } from 'react'
import { Button, Buttons, Icon } from 'styles'
import { ON, STATE } from 'machine/article'
import CANCEL from 'assets/cancel.svg'
import SAVE from 'assets/save.svg'

const Edit = (props): ReactElement => (
  <Buttons>
    <Button
      onClick={() => props.send(ON.CANCEL)}
      disabled={!props.isAllowed(ON.CANCEL)}>
      <Icon src={CANCEL} />
    </Button>
    <Button
      onClick={() => props.send(ON.SUBMIT, { articleId: props.articleId })}
      loading={props.state === STATE.LOADING && true}
      disabled={!props.isAllowed(ON.SUBMIT)}>
      <Icon src={SAVE} />
    </Button>
  </Buttons>
)

export default Edit