import { Button, Buttons, Icon } from 'styles'
import React from 'react'
import { ON, STATE } from 'machine/article'
import EDIT from 'assets/edit.svg'
import CANCEL from 'assets/cancel.svg'
import SAVE from 'assets/save.svg'

const Edit = (props) => {
  const {send, isAllowed, isEdit, state, articleId} = props
  if (isEdit) return (
    <Buttons>
      <Button
        onClick={() => send(ON.CANCEL)}
        disabled={!isAllowed(ON.CANCEL)}>
        <Icon src={CANCEL} />
      </Button>
      <Button
        onClick={() => send(ON.SUBMIT, { articleId })}
        loading={state === STATE.LOADING ? true : undefined}
        disabled={!isAllowed(ON.SUBMIT)}>
        <Icon src={SAVE} />
      </Button>
    </Buttons>
  )
  return (
    <Button onClick={() => send(ON.EDIT)}>
      <Icon src={EDIT}/>
    </Button>
  )
}

export default Edit