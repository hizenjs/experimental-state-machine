import { Avatar, Job, Name, Select, User, Writer } from 'styles'
import { User as UserType } from 'types'
import { users } from 'server/data'
import { ON } from 'machine/article'
import React from 'react'

const Author = (props) => {
  const { article, isEdit, send, index } = props
  if (isEdit) return (
    <Select
      defaultValue={users.findIndex((user: UserType) => JSON.stringify(user) === JSON.stringify(article.author))}
      onChange={({ target }) =>
        send(ON.CHANGE_AUTHOR, { value: users[target.value], index })
      }>
      {users.map(({ name, job }, index) =>
        <option key={name} value={index}>{name} - {job}</option>
      )}
    </Select>
  )
  return (
    <Writer>
      <Avatar src={article.author.avatar} alt={article.author.name}/>
      <User><Name>{article.author.name}</Name><Job>{article.author.job}</Job></User>
    </Writer>
  )
}
export default Author