import React, { ChangeEvent, ReactElement } from 'react'
import styled, { StyledComponent } from 'styled-components'
import AIRPLAY from 'assets/airplay.svg'

const Container = styled.div`
  margin-top: 100px;
  background: white;
  padding: 10px;
  border-radius: 4px;
  display: flex;
  align-items: center;
  box-shadow: 0 12px 12px rgba(0, 0, 0, 0.6);
  transition: 300ms;
  :hover {
    box-shadow: 0 6px 6px rgba(0, 0, 0, 1);
  }
`

const Icon = styled.img`
  width: 30px;
  height: 30px;
  border-radius: 4px;
  margin: 0 10px;
  margin-right: 5px;
`

const Text: StyledComponent<any, HTMLInputElement> = styled.input`
  font-size: 18px;
  border: none;
  margin-left: 10px;
  padding: 8px 10px;
  font-weight: bold;
  border-radius: 4px;
  color: #2b2b2b;
  :focus {
    outline: none;
    background: #e5e5e5;
  }
`

const Button = styled.button`
  margin-left: 10px;
  font-size: 16px;
  font-weight: bold;
  border: none;
  background: #5ca04e;
  color: white;
  border-radius: 4px;
  height: 36px;
  width: 120px;
  transition: 300ms;
  :hover {
    background: rgb(51, 111, 41);
  }
`

export default function Card (): ReactElement {
  const [state, setState] = React.useState<Record<string, unknown>>({ ip: '192.168.1.32:7878', editable: false })

  function handleClick () {
    if (state.editable) setTimeout(() => setState({...state, editable: false}), 2000)
    else setState({...state, editable: true})

  }

  return (
    <Container>
      <Icon src={AIRPLAY} />
      <Text
        type="text"
        value={state.ip}
        readOnly={!state.editable}
        onChange={(ip: ChangeEvent<HTMLInputElement>) => setState({...state, ip: ip.target.value})}
      />
      <Button onClick={() => handleClick()}>{state.editable ? 'Save' : 'Edit'}</Button>
    </Container>
  )
}
