import React from 'react'
import ReactDOM from 'react-dom'
import { Normalize } from 'styled-normalize'
import { data } from 'server/data'
import Article from 'components/Article'
import Heading from 'components/Head'
import Main from 'layouts/Main'
import 'machine/article'

ReactDOM.render(
  <React.StrictMode>
    <Normalize />
    <Main>
      <Heading title={'react'} subtitle={'Finite State Machine'} />
      {Object.values(data).map(({ id }, index) => <Article key={id} articleId={id} index={index} />)}
    </Main>
  </React.StrictMode>,
  document.getElementById('root')
)
