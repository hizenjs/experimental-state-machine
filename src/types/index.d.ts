import { ReactElement } from 'react'
import { STATE } from 'enums/machine'

export type User = {
  name: string,
  avatar: string,
  job: string,
}

export type Types = 'Mobile' | 'Hardware' | 'Server' | 'System' | 'Website'

export type Article = {
  id: string,
  title: string,
  date: string,
  category: Types | string,
  illustration: string,
  author: User
}

export type Context = { [k:string]: Article }

export type EventKeys = { value: any, data: any, index?: string }

export type Event = { [K in keyof EventKeys]: any }

export type UseMachine = {
  state: STATE
  send: (type: string, params?: { [k: string]: unknown }) => void
  isAllowed: (action: string) => boolean
  context: Context
}

export type ArticleProps = { articleId: string, index: number }

export interface ArticleComponent extends ReactElement {}