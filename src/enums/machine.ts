export enum STATE {
  IDLE = 'idle',
  EDIT = 'edit',
  LOADING = 'loading',
  SUCCESS = 'success'
}

export enum ON {
  CANCEL = 'cancel',
  SUBMIT = 'submit',
  ERROR = 'error',
  DONE = 'done',
  EDIT = 'edit',
  CHANGE_CATEGORY = 'category',
  CHANGE_AUTHOR = 'author',
  CHANGE_TITLE = 'title'
}