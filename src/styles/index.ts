import styled, { css, keyframes } from 'styled-components'

export type ContainerProps = {
  flip: boolean
}

type ButtonProps = {
  loading?: boolean
}

const Rotate = keyframes`
  0% { transform: rotate(0) }
  100% { transform: rotate(360deg) }
`

const Flip = keyframes`
  0% { transform: rotateX(0deg) scale(1.05); }
  100% { transform: rotateX(90deg) scale(1.05); }
`

export const Container = styled.div`
  width: 774px;
  height: 233px;
  border: 2px solid #444444;
  border-radius: 13px;
  padding: 18px;
  display: flex;
  box-shadow: 0 6px 12px rgba(0, 0, 0, 0.2);
  margin: 12px 24px;
  transition: 200ms;
  animation: ${({flip}:ContainerProps) => flip && Flip};
  animation-timing-function: ease-in-out;
  animation-direction: alternate;
  animation-iteration-count: 2;
  animation-duration: 0.1s;
  :hover {
    transform: scale(1.05);
  }
`
export const Avatar = styled.img`
  width: 36px;
  height: 36px;
  object-fit: cover;
  border-radius: 36px;
`
export const Cover = styled.img`
  width: 348px;
  //height: 232px;
  background: #444444;
  border-radius: 13px;
  object-fit: cover;
  cursor: pointer;
`

export const Content = styled.div`
  margin-left: 18px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
`
export const Writer = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`
export const Info = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  cursor: default;
`
export const User = styled.div`
  margin-left: 12px;
`

export const Icon = styled.img`
  width: 18px;
`

export const Buttons = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row-reverse;
  width: calc(38px *2);
  :disabled, [disabled] {
    opacity: .5;
  }
`

const AnimLoading = css`
  animation: ${Rotate} 0.5s ease-in-out infinite;
`

export const Button = styled.button`
  width: 36px;
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 36px;
  background: #444444;
  cursor: pointer;
  transition: 200ms;
  border: none;
  ${({loading}: ButtonProps) => loading && AnimLoading}
  :hover {
    animation: ${Rotate} 0.5s ease-in-out;
  }
`
export const Top = styled.div`
  margin-top: 2px;
  width: 100%;
  display: flex;
  justify-content: space-between;
`

export const Date = styled.span`
  margin-top: 8px;
  font-size: 12px;
  font-weight: 400;
  opacity: 0.6;
`

export const Title = styled.h1`
  font-size: 24px;
  text-align: left;
  cursor: pointer;
  width: 400px;
`

export const Input = styled.textarea`
  font-size: 24px;
  text-align: left;
  cursor: pointer;
  background: transparent;
  font-weight: bold;
  color: white;
  width: 400px;
  resize: none;
`

export const Type = styled.h6`
  font-size: 12px;
  font-weight: 400;
  margin: 0;
`

export const Select = styled.select`
  font-size: 12px;
  font-weight: 400;
  margin: 0;
  background: transparent;
  border: none;
  color: white;
  :focus {
    outline: none;
  }
  & option {
    background: #444444;
    color: white;
  }
`

export const Name = styled.h4`
  font-size: 12px;
  text-align: left;
  margin: 0;
  padding-bottom: 4px;
`
export const Job = styled.h5`
  font-weight: 400;
  font-size: 10px;
  text-align: left;
  opacity: 0.6;
  margin: 0;
`

export const State = styled.div`
  position: fixed;
  z-index: 99;
  top: 18px;
  left: 18px;
  text-transform: uppercase;
  padding: 5px;
  background: white;
  border-radius: 13px 0;
  width: 100px;
  color: black;
  font-weight: bold;
`