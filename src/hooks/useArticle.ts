import { useCallback } from 'react'
import { Article } from 'types'

export default function useArticle(list: { [k:string]:Article }, selected: string): Article {
  const getArticle = useCallback((): Article => {
    return Object.values(list).find(article => article.id === selected)
  }, [list, selected])

  return getArticle()
}