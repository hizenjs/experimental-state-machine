import { useCallback, useRef, useState } from 'react'
import { interpret, Machine, Service } from 'robot3'
import { UseMachine, Context } from 'types'
import { STATE } from 'machine/article'

export default function useMachine(machine: Machine):UseMachine {
  //Listen to machine reference
  const { current: instance } = useRef<Service<typeof machine>>(interpret(machine, () => {
    setState(STATE[instance.machine.current.toUpperCase()])
    setContext(instance.context)
  }));

  // Return current machine state
  const [state, setState] = useState<STATE>(STATE[instance.machine.current.toUpperCase()])
  const [context, setContext] = useState<Context>(instance.context)

  // Call a transition with some params only if machine state change
  const send = useCallback((type: string, params: {[k:string]: unknown} = {}) =>
    instance.send({type, ...params}),
    [instance])

  // Return boolean if action is in current state transitions
  const isAllowed = useCallback((action): boolean => {
    const transitions = instance.machine.state.value.transitions

    // Every action which are not in the current state will return false
    if (!transitions.has(action)) return false

    // Get the transition name relative to action ('enum ON')
    const actionTransitions = transitions.get(action)
    if (!actionTransitions) return false

    // Checking for transition guards
    for (const t of actionTransitions) {
      // @ts-ignore TODO: Type guards from R3
      if (t.guards(instance.context) || !t.guards) return true
    }

    return false
  }, [instance.machine.state.value.transitions, instance.context])

  return { state, send, isAllowed, context }
}
