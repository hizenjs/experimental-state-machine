import {
  createMachine,
  invoke,
  reduce,
  state,
  transition,
  Machine,
  guard, immediate,
} from 'robot3'
import { Context, Event } from 'types'
import { STATE, ON} from 'enums/machine'
import { data } from 'server/data'
import fakeServer from 'server'

/* <S,C,K => ?initialState: string, {[k:string]: StateMachine}, ?() => Context }> */
const schema: Machine = createMachine(STATE.IDLE, {
  idle: state(
    transition(ON.EDIT, STATE.EDIT)
  ),
  edit: state(
    transition(ON.CANCEL, STATE.IDLE),
    transition(ON.CHANGE_TITLE, STATE.EDIT,
      reduce((context: Context, event: Event) =>
        ({ ...context, [event.index]: {...context[event.index], title: event.value} }))
    ),
    transition(ON.CHANGE_CATEGORY, STATE.EDIT,
      reduce((context: Context, event: Event) =>
        ({ ...context, [event.index]: {...context[event.index], category: event.value} }))
    ),
    transition(ON.CHANGE_AUTHOR, STATE.EDIT,
      reduce((context: Context, event: Event) =>
        ({ ...context, [event.index]: {...context[event.index], author: event.value} }))
    ),
    transition(ON.SUBMIT, STATE.LOADING,
      guard((context: Context) => (context !== data))
    )
  ),
  success: state(immediate(STATE.IDLE)),
  loading: invoke(
    async (context: Context) => await fakeServer(context, 2000).then((resolve:Context) => resolve),
    transition(ON.ERROR, STATE.EDIT),
    transition(ON.DONE, STATE.SUCCESS,
      reduce((context: Context, event: Event) => event.data)
    ),
  ),
}, ():Context => ({ ...data }))

export { STATE, ON }
export default schema
