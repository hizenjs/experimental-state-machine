# Finite State Machine



## Explore

- [ ]  Why Finite State Machine

- [ ]  Library

- [ ]  Composition

- [ ]  Workflow

- [ ]  Context

- [ ]  Link to React with or without R3-React

- [ ]  Example

- [ ]  Advantage



### Introduction

User interfaces can be expressed by two things:

- The state of the UI

- Actions that can change that state

From credit card payment devices and gas pump screens to the software that your company creates, user interfaces react to the actions of the user and other sources and change their state accordingly. This concept isn’t just limited to technology, it’s a fundamental part of how *everything* works

> For every action, there is an equal and opposite reaction.
> 
> – Isaac Newton

This is a concept we can apply to developing better user interfaces, but before we go there, I want you to try something. Consider a photo gallery interface with this user interaction flow:

1. Show a **search input** and a **search button** that allows the user to search for photos

2. Display the search results in a grid of small sized photos

3. When a photo is clicked/tapped, show the full size photo

4. When a full-sized photo is clicked/tapped again, go back to the gallery view

Now think about how you would develop it. Maybe even try programming it in React. I’ll wait; I’m just an article. I’m not going anywhere.

Finished? Awesome! That wasn’t too difficult, right? Now think about the following scenarios that you might have forgotten:

- What if the user clicks the search button repeatedly?
- What if the user wants to cancel the search while it’s in-flight?
- Is the search button disabled while searching?
- What if the user mischievously enables the disabled button?
- Is there any indication that the results are loading?
- What happens if there’s an error? Can the user retry the search?
- What if the user searches and then clicks a photo? What should happen?

These are just *some* of the potential problems that can arise during planning, development, or testing. Few things are worse in software development than thinking that you’ve covered every possible use case, and then discovering (or receiving) new edge cases that will further complicate your code once you account for them. It’s especially difficult to jump into a pre-existing project where all of these use cases are undocumented, but instead hidden in spaghetti code and left for you to decipher.

What if we could determine all possible UI states that can result from all possible actions performed on each state? And what if we can visualize these states, actions, and transitions between states? Designers intuitively do this, in what are called “user flows” (or “UX Flows”), to depict what the next state of the UI should be depending on the user interaction.

In computer science terms, there is a computational model called [finite automata](https://en.wikipedia.org/wiki/Finite-state_machine), or “finite state machines” (FSM), that can express the same type of information. That is, they describe which state comes next when an action is performed on the current state. Just like user flows, these finite state machines can be visualized in a clear and unambiguous way. For example, here is the state transition diagram describing the FSM of a traffic light:



![https://i2.wp.com/css-tricks.com/wp-content/uploads/2017/11/stoplight-fsm.png?ssl=1](https://i2.wp.com/css-tricks.com/wp-content/uploads/2017/11/stoplight-fsm.png?ssl=1)

![https://www.idalko.com/wp-content/uploads/2018/06/A-guide-to-Jira-workflow-best-practices-e1591353492879-846x435.png](https://www.idalko.com/wp-content/uploads/2018/06/A-guide-to-Jira-workflow-best-practices-e1591353492879-846x435.png)

### What is a Finite State Machine

A state machine is a useful way of modeling behavior in an application: for every action, there is a reaction in the form of a state change. There’s 5 parts to a classical finite state machine:

1. A set of states (e.g., `idle`, `loading`, `success`, `error`, etc.)
2. A set of actions (e.g., `SEARCH`, `CANCEL`, `SELECT_PHOTO`, etc.)
3. An initial state (e.g., `idle`)
4. A transition function (e.g., `transition('idle', 'SEARCH') == 'loading'`)
5. Final states (which don’t apply to this article.)



[Trafic light FSM Example](https://stately.ai/registry/user/50c3a353-f1c6-42bb-9b5b-c2040e989dba)

[Data List FSM Example](https://stately.ai/registry/user/50c3a353-f1c6-42bb-9b5b-c2040e989dba)



Deterministic finite state machines (which is what we’ll be dealing with) have some constraints, as well:

- There are a finite number of possible states
- There are a finite number of possible actions (these are the “finite” parts)
- The application can only be in one of these states at a time
- Given a `currentState` and an `action`, the transition function must always return the same `nextState` (this is the “deterministic” part)



A finite state machine can be represented as a mapping from a `state` to its “transitions”, where each transition is an `action` and the `nextState` that follows that action. This mapping is just a plain JavaScript object.

Let’s consider an American traffic light example, one of the simplest FSM examples. Assume we start on `green`, then transition to `yellow` after some `TIMER`, and then `RED` after another `TIMER`, and then back to `green` after another `TIMER`:

```javascript
const machine = {
  green: { TIMER: 'yellow' },
  yellow: { TIMER: 'red' },
  red: { TIMER: 'green' }
};
const initialState = 'green';
```

A **transition function** answers the question:

**Given the current state and an action, what will the next state be?**

With our setup, transitioning to the next state based on an action (in this case, `TIMER`) is just a look-up of the `currentState` and `action` in the `machine` object, since:

- `machine[currentState]` gives us the next action mapping, e.g.: `machine['green'] == {TIMER: 'yellow'}`
- `machine[currentState][action]` gives us the next state from the action, e.g.: `machine['green']['TIMER'] == 'yellow'`:

```javascript
// ...
function transition(currentState, action) {
  return machine[currentState][action];
}

transition('green', 'TIMER');
// => 'yellow'
```

Instead of using `if/else` or `switch` statements to determine the next state, e.g., `if (currentState === 'green') return 'yellow';`, we moved all of that logic into a plain JavaScript object that can be serialized into JSON. That’s a strategy that will pay off greatly in terms of testing, visualization, reuse, analysis, flexibility, and configurability.



### Finite State Machines in React

Taking a look at a more complicated example, let’s see how we can represent our gallery app using a finite state machine. The app can be in one of several states:

- `start` – the initial search page view
- `loading` – search results fetching view
- `error` – search failed view
- `gallery` – successful search results view
- `photo` – detailed single photo view

And several actions can be performed, either by the user or the app itself:

- `SEARCH` – user clicks the “search” button
- `SEARCH_SUCCESS` – search succeeded with the queried photos
- `SEARCH_FAILURE` – search failed due to an error
- `CANCEL_SEARCH` – user clicks the “cancel search” button
- `SELECT_PHOTO` – user clicks a photo in the gallery
- `EXIT_PHOTO` – user clicks to exit the detailed photo view

The best way to visualize how these states and actions come together, at first, is with two very powerful tools: pencil and paper. Draw arrows between the states, and label the arrows with actions that cause transitions between the states:

We can now represent these transitions in an object, just like in the traffic light example:

```javascript
const galleryMachine = {
  start: {
    SEARCH: 'loading'
  },
  loading: {
    SEARCH_SUCCESS: 'gallery',
    SEARCH_FAILURE: 'error',
    CANCEL_SEARCH: 'gallery'
  },
  error: {
    SEARCH: 'loading'
  },
  gallery: {
    SEARCH: 'loading',
    SELECT_PHOTO: 'photo'
  },
  photo: {
    EXIT_PHOTO: 'gallery'
  }
};

const initialState = 'start';
```

Now let’s see how we can incorporate this finite state machine configuration and the transition function into our gallery app. In the `App`‘s component state, there will be a single property that will indicate the current finite state, `gallery`:

```javascript
export default function App() {
  const [state, setState] = useState({
    gallery: 'start', // initial finite state
    query: '',
    items: []
  })
}
  // ..    .
```

The `transition` function will be a method of this `App` class, so that we can retrieve the current finite state:

```javascript
  // ...
  const transition = (action) => {
    const currentGalleryState = state.gallery;
    const nextGalleryState =
      galleryMachine[currentGalleryState][action.type];

    if (nextGalleryState) {
      const nextState = command(nextGalleryState, action);

     setState({
        gallery: nextGalleryState,
        ...nextState // extended state
      });
    }
  }
  // ...
```

This looks similar to the previously described `transition(currentState, action)` function, with a few differences:

- The `action` is an object with a `type` property that specifies the string action type, e.g., `type: 'SEARCH'`
- Only the `action` is passed in since we can retrieve the current finite state from `this.state.gallery`
- The entire app state will be updated with the next finite state, i.e., `nextGalleryState`, as well as any *extended state* (`nextState`) that results from executing a command based on the next state and action payload (see the “Executing commands” section)

### Executing commands

When a state change occurs, “side effects” (or “commands” as we’ll refer to them) might be executed. For example, when a user clicks the “Search” button and a `'SEARCH'` action is emitted, the state will transition to `'loading'`, and an async Flickr search should be executed (otherwise, `'loading'` would be a lie, and developers should never lie).

We can handle these side effects in a `command(nextState, action)` method that determines what to execute given the next finite state and action payload, as well as what the *extended state* should be:



### Comparison Robot3 vs XState

[XState](https://xstate.js.org/) is a popular JavaScript library for building Finite State Machines and [Statecharts](https://statecharts.github.io/) that was an inspiration for creating [Robot](https://thisrobot.life/). Since most people will be choosing to use either XState or Robot, we think it's important to list the various tradeoffs each make. Of course this guide will be biased in favor of Robot's tradeoffs, but we attempt to be fair about them.

XState and Robot share similarities that make them standout from other, more minimal, Finite State Machine libraries:

- Extended state (context) makes it possible to keep non-state values within the machine.
- Both can [invoke](https://www.w3.org/TR/scxml/#invoke) external functions as well as other state machines to perform subtasks.
- Both support [guards](https://thisrobot.life/api/guard.html) to prevent state transitions from occurring on condition.

Below goes into more detail of the various tradeoffs each makes.



Options objects are familiar APIs but have some downsides too. In the above you'll notice that some property keys, like `inactive`, `active`, and `TOGGLE` are domain specific information, whereas some other keys like `states`, `initial`, and `on` are options of the machine. This blending of your stuff with the library's stuff makes it a little harder to read, especially as machines grow.

More importantly, options objects aren't particularly [composable](https://en.wikipedia.org/wiki/Function_composition). A fundamental design constraint of Robot is to aid with [composition](https://thisrobot.life/guides/composition.html). The above machine would be written as:



### Serialization / Deserialization

XState's machines can be [serialized to JSON](https://xstate.js.org/docs/guides/states.html#persisting-state). This is useful if you want to preserve state.

It's possible to do the same with Robot, but currently it's a little more cumbersome to do. We might add better APIs for serializing state in the future, if it's something people want. Currently you can do so like so:



## Updating context

In Robot the context is updated using [reduce](https://thisrobot.life/api/reduce.html), which is similar to how state is managed in [Redux](https://redux.js.org/).



In XState the context object is updated through a special [assign](https://xstate.js.org/docs/guides/context.html#updating-context-with-assign) operator. It can work like reduce, but also can take a key to update only 1 value on the context, leaving the others alone.



## Parallel states

XState supports the Statecharts feature known as [parallel states](https://xstate.js.org/docs/guides/parallel.html). A parallel state machine is one in which the machine can be in multiple states at the same time. You change the states separately and they never affect each other.

An example would be a rich text editor with bold, italic, and underline states. In XState you would write that as:



Robot does not support parallel state machines. Since the states do not affect other states, these are essentially 3 separate machines. In keeping with having 1 way of doing things, we prefer to model these as 3 machines:



## Final state

In Statecharts, a final state is a state that can not be transitioned away from. Both XState and Robot support final states. In XState this is by setting the state's `type`:



Since robot uses functions for defining machines, you can simply add a state with no transitions. Since there are no transitions that state is final. I like to alias `state` to `final` to make the final states more obvious:



## Actors

XState supports the actor model as an alternative to invoking machines. It's very similar to invoke, except that it doesn't hang off of a state like invoked machines do. Because the use-case is so similar, Robot just sticks with `invoke` for now. I am interested in different ways to spawn new machines, so this is an idea I'm keeping my eye on.

### Delayed transitions

XState supports [delayed transitions](https://xstate.js.org/docs/guides/delays.html), essentially a timeout until moving to another state. In keeping with having one way to do things, in Robot you can just use [invoke](https://thisrobot.life/api/invoke.html) like so:

Because of composition this can be shortened:

## Visualization

XState comes with a [visualizer tool](https://xstate.js.org/viz/) that gives a visual representation of the state machine, and allows you to step through various states and see what the intermediate states look like. Some find this to be a useful way to get the big picture of how the state machine operates. Robot does not have a visualization tool at this time.

### Spec Conformity

XState conforms to the [SCXML](https://www.w3.org/TR/scxml/) specification. Conforming to a standard has the advantage that it is theoretically possible to import and export the XML so that a single machine can be used in multiple languages.

Robot does not conform to the SCXML spec, but rather is inspired by many of the features it includes. I don't believe that reusing machines across languages is very compelling and am focused on the use-cases within web applications.


